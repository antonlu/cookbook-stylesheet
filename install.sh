#!/bin/bash

TEX_DIR=`kpsewhich -var-value=TEXMFHOME`

if [[ -z $TEX_DIR ]]; then
  echo "TEXMFHOME could not be found. Latex is probably not installed. Exiting."
  return 1
fi

TEX_DIR=$TEX_DIR/tex/custom

mkdir -p $TEX_DIR
\cp cookbook.sty $TEX_DIR/
sudo \cp -r fonts/* /usr/share/fonts/

sudo fc-cache -fv

